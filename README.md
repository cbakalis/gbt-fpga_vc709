In order for the VC709's FPGA to connect to FELIX, an external clock board was used. The clock board generates a low-jitter clock (120.237 MHz) for the GBT-FPGA's MGT, and uses as a reference the same clock that FELIX uses. 

This reference clock comes from an L1DDC (GBTx), that outputs the TTC clock from FELIX to a miniSAS (twinax) cable. The miniSAS cable is then connected to an miniSAS-to-SMA breaker board. An SMA differential pair delivers the reference clock to the clock board.

![alt text](http://oi66.tinypic.com/1zqdj03.jpg)