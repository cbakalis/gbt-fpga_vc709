# Set the reference directory for source file relative paths (by default the value is script directory path)
set origin_dir [file dirname [info script]]

# Use origin directory path location variable, if specified in the tcl shell
if { [info exists ::origin_dir_loc] } {
  set origin_dir $::origin_dir_loc
}

puts $origin_dir/../../

# Set the project name
set project_name "gbt-fpga_vc709"

# Use project name variable, if specified in the tcl shell
if { [info exists ::user_project_name] } {
  set project_name $::user_project_name
}

variable script_file
set script_file "build_vc709.tcl"

# Help information for this script
proc help {} {
  variable script_file
  puts "\nDescription:"
  puts "Recreate a Vivado project from this script. The created project will be"
  puts "functionally equivalent to the original project for which this script was"
  puts "generated. The script contains commands for creating a project, filesets,"
  puts "runs, adding/importing sources and setting properties on various objects.\n"
  puts "Syntax:"
  puts "$script_file"
  puts "$script_file -tclargs \[--origin_dir <path>\]"
  puts "$script_file -tclargs \[--project_name <name>\]"
  puts "$script_file -tclargs \[--help\]\n"
  puts "Usage:"
  puts "Name                   Description"
  puts "-------------------------------------------------------------------------"
  puts "\[--origin_dir <path>\]  Determine source file paths wrt this path. Default"
  puts "                       origin_dir path value is \".\", otherwise, the value"
  puts "                       that was set with the \"-paths_relative_to\" switch"
  puts "                       when this script was generated.\n"
  puts "\[--project_name <name>\] Create project with the specified name. Default"
  puts "                       name is the name of the project from where this"
  puts "                       script was generated.\n"
  puts "\[--help\]               Print help information for this script"
  puts "-------------------------------------------------------------------------\n"
  exit 0
}

if { $::argc > 0 } {
  for {set i 0} {$i < [llength $::argc]} {incr i} {
    set option [string trim [lindex $::argv $i]]
    switch -regexp -- $option {
      "--origin_dir"   { incr i; set origin_dir [lindex $::argv $i] }
      "--project_name" { incr i; set project_name [lindex $::argv $i] }
      "--help"         { help }
      default {
        if { [regexp {^-} $option] } {
          puts "ERROR: Unknown option '$option' specified, please type '$script_file -tclargs --help' for usage info.\n"
          return 1
        }
      }
    }
  }
}

# Create project
create_project $project_name $origin_dir/../../$project_name -part xc7vx690tffg1761-2

# Set the directory path for the new project
set proj_dir [get_property directory [current_project]]

# Reconstruct message rules
# None

# Set project properties
set obj [current_project]
set_property -name "board_connections" -value "" -objects $obj
set_property -name "board_part" -value "xilinx.com:vc709:part0:1.8" -objects $obj
set_property -name "compxlib.activehdl_compiled_library_dir" -value "$proj_dir/${project_name}.cache/compile_simlib/activehdl" -objects $obj
set_property -name "compxlib.funcsim" -value "1" -objects $obj
set_property -name "compxlib.ies_compiled_library_dir" -value "$proj_dir/${project_name}.cache/compile_simlib/ies" -objects $obj
set_property -name "compxlib.modelsim_compiled_library_dir" -value "$proj_dir/${project_name}.cache/compile_simlib/modelsim" -objects $obj
set_property -name "compxlib.overwrite_libs" -value "0" -objects $obj
set_property -name "compxlib.questa_compiled_library_dir" -value "$proj_dir/${project_name}.cache/compile_simlib/questa" -objects $obj
set_property -name "compxlib.riviera_compiled_library_dir" -value "$proj_dir/${project_name}.cache/compile_simlib/riviera" -objects $obj
set_property -name "compxlib.timesim" -value "1" -objects $obj
set_property -name "compxlib.vcs_compiled_library_dir" -value "$proj_dir/${project_name}.cache/compile_simlib/vcs" -objects $obj
set_property -name "compxlib.xsim_compiled_library_dir" -value "" -objects $obj
set_property -name "corecontainer.enable" -value "1" -objects $obj
set_property -name "default_lib" -value "xil_defaultlib" -objects $obj
set_property -name "dsa.num_compute_units" -value "16" -objects $obj
set_property -name "dsa.rom.debug_type" -value "0" -objects $obj
set_property -name "dsa.rom.prom_type" -value "0" -objects $obj
set_property -name "enable_optional_runs_sta" -value "0" -objects $obj
set_property -name "generate_ip_upgrade_log" -value "1" -objects $obj
set_property -name "ip_cache_permissions" -value "read write" -objects $obj
set_property -name "ip_interface_inference_priority" -value "" -objects $obj
set_property -name "ip_output_repo" -value "$proj_dir/${project_name}.cache/ip" -objects $obj
set_property -name "managed_ip" -value "0" -objects $obj
set_property -name "pr_flow" -value "0" -objects $obj
set_property -name "sim.ip.auto_export_scripts" -value "1" -objects $obj
set_property -name "sim.use_ip_compiled_libs" -value "1" -objects $obj
set_property -name "simulator_language" -value "Mixed" -objects $obj
set_property -name "source_mgmt_mode" -value "All" -objects $obj
set_property -name "target_language" -value "VHDL" -objects $obj
set_property -name "target_simulator" -value "XSim" -objects $obj
set_property -name "xpm_libraries" -value "XPM_CDC XPM_MEMORY" -objects $obj
set_property -name "xsim.array_display_limit" -value "1024" -objects $obj
set_property -name "xsim.radix" -value "hex" -objects $obj
set_property -name "xsim.time_unit" -value "ns" -objects $obj
set_property -name "xsim.trace_limit" -value "65536" -objects $obj

# Create 'sources_1' fileset (if not found)
if {[string equal [get_filesets -quiet sources_1] ""]} {
  create_fileset -srcset sources_1
}

# top
read_vhdl -library work $origin_dir/../../sources/src/gbt_fpga_vc709_top.vhd

# misc
read_vhdl -library work $origin_dir/../../sources/src/misc/debouncer.vhd
read_vhdl -library work $origin_dir/../../sources/src/misc/CDCC.vhd

# mgt_main
read_vhdl -library work $origin_dir/../../sources/src/mgt_gbt_fpga/gbt_fpga_wrapper.vhd
read_vhdl -library work $origin_dir/../../sources/src/mgt_gbt_fpga/gbt_bank_reset.vhd
read_vhdl -library work $origin_dir/../../sources/src/mgt_gbt_fpga/gbt_bank.vhd
read_vhdl -library work $origin_dir/../../sources/src/mgt_gbt_fpga/gbt_pattern_checker.vhd
read_vhdl -library work $origin_dir/../../sources/src/mgt_gbt_fpga/gbt_pattern_generator.vhd
read_vhdl -library work $origin_dir/../../sources/src/mgt_gbt_fpga/gbt_pattern_matchflag.vhd
read_vhdl -library work $origin_dir/../../sources/src/mgt_gbt_fpga/gbt_rx_decoder_gbtframe_chnsrch.vhd
read_vhdl -library work $origin_dir/../../sources/src/mgt_gbt_fpga/gbt_rx_decoder_gbtframe_deintlver.vhd
read_vhdl -library work $origin_dir/../../sources/src/mgt_gbt_fpga/gbt_rx_decoder_gbtframe_elpeval.vhd
read_vhdl -library work $origin_dir/../../sources/src/mgt_gbt_fpga/gbt_rx_decoder_gbtframe_errlcpoly.vhd
read_vhdl -library work $origin_dir/../../sources/src/mgt_gbt_fpga/gbt_rx_decoder_gbtframe_lmbddet.vhd
read_vhdl -library work $origin_dir/../../sources/src/mgt_gbt_fpga/gbt_rx_decoder_gbtframe_rs2errcor.vhd
read_vhdl -library work $origin_dir/../../sources/src/mgt_gbt_fpga/gbt_rx_decoder_gbtframe_rsdec.vhd
read_vhdl -library work $origin_dir/../../sources/src/mgt_gbt_fpga/gbt_rx_decoder_gbtframe_syndrom.vhd
read_vhdl -library work $origin_dir/../../sources/src/mgt_gbt_fpga/gbt_rx_decoder.vhd
read_vhdl -library work $origin_dir/../../sources/src/mgt_gbt_fpga/gbt_rx_descrambler_16bit.vhd
read_vhdl -library work $origin_dir/../../sources/src/mgt_gbt_fpga/gbt_rx_descrambler_21bit.vhd
read_vhdl -library work $origin_dir/../../sources/src/mgt_gbt_fpga/gbt_rx_descrambler.vhd
read_vhdl -library work $origin_dir/../../sources/src/mgt_gbt_fpga/gbt_rx_framealigner_bscounter.vhd
read_vhdl -library work $origin_dir/../../sources/src/mgt_gbt_fpga/gbt_rx_framealigner_pattsearch.vhd
read_vhdl -library work $origin_dir/../../sources/src/mgt_gbt_fpga/gbt_rx_framealigner_rightshift.vhd
read_vhdl -library work $origin_dir/../../sources/src/mgt_gbt_fpga/gbt_rx_framealigner.vhd
read_vhdl -library work $origin_dir/../../sources/src/mgt_gbt_fpga/gbt_rx_framealigner_wraddr.vhd
read_vhdl -library work $origin_dir/../../sources/src/mgt_gbt_fpga/gbt_rx_frameclk_phalgnr.vhd
read_vhdl -library work $origin_dir/../../sources/src/mgt_gbt_fpga/gbt_rx_gearbox_latopt.vhd
read_vhdl -library work $origin_dir/../../sources/src/mgt_gbt_fpga/gbt_rx_gearbox_std_rdctrl.vhd
read_vhdl -library work $origin_dir/../../sources/src/mgt_gbt_fpga/gbt_rx_gearbox_std.vhd
read_vhdl -library work $origin_dir/../../sources/src/mgt_gbt_fpga/gbt_rx_gearbox.vhd
read_vhdl -library work $origin_dir/../../sources/src/mgt_gbt_fpga/gbt_rx_status.vhd
read_vhdl -library work $origin_dir/../../sources/src/mgt_gbt_fpga/gbt_rx.vhd
read_vhdl -library work $origin_dir/../../sources/src/mgt_gbt_fpga/gbt_tx_encoder.vhd
read_vhdl -library work $origin_dir/../../sources/src/mgt_gbt_fpga/gbt_tx_gearbox_latopt.vhd
read_vhdl -library work $origin_dir/../../sources/src/mgt_gbt_fpga/gbt_tx_gearbox_phasemon.vhd
read_vhdl -library work $origin_dir/../../sources/src/mgt_gbt_fpga/gbt_tx_gearbox_std_rdwrctrl.vhd
read_vhdl -library work $origin_dir/../../sources/src/mgt_gbt_fpga/gbt_tx_gearbox_std.vhd
read_vhdl -library work $origin_dir/../../sources/src/mgt_gbt_fpga/gbt_tx_gearbox.vhd
read_vhdl -library work $origin_dir/../../sources/src/mgt_gbt_fpga/gbt_tx.vhd
read_vhdl -library work $origin_dir/../../sources/src/mgt_gbt_fpga/mgt_latopt_bitslipctrl.vhd
read_vhdl -library work $origin_dir/../../sources/src/mgt_gbt_fpga/multi_gigabit_transceivers.vhd
read_vhdl -library work $origin_dir/../../sources/src/mgt_gbt_fpga/phaligner_mmcm_controller.vhd
read_vhdl -library work $origin_dir/../../sources/src/mgt_gbt_fpga/xlx_k7v7_gbt_example_design.vhd
read_vhdl -library work $origin_dir/../../sources/src/mgt_gbt_fpga/xlx_k7v7_gbt_tx_gearbox_std_dpram.vhd
read_vhdl -library work $origin_dir/../../sources/src/mgt_gbt_fpga/xlx_k7v7_phalgnr_std_mmcm.vhd
read_vhdl -library work $origin_dir/../../sources/src/mgt_gbt_fpga/gbt_tx_scrambler.vhd
read_vhdl -library work $origin_dir/../../sources/src/mgt_gbt_fpga/gbt_tx_scrambler_21bit.vhd
read_vhdl -library work $origin_dir/../../sources/src/mgt_gbt_fpga/gbt_tx_encoder_gbtframe_polydiv.vhd
read_vhdl -library work $origin_dir/../../sources/src/mgt_gbt_fpga/gbt_tx_encoder_gbtframe_rsencode.vhd
read_vhdl -library work $origin_dir/../../sources/src/mgt_gbt_fpga/gbt_tx_encoder_gbtframe_intlver.vhd
read_vhdl -library work $origin_dir/../../sources/src/mgt_gbt_fpga/xlx_k7v7_gbt_rx_gearbox_std_dpram.vhd
read_vhdl -library work $origin_dir/../../sources/src/mgt_gbt_fpga/mgt_latopt_bitslipctrl.vhd
read_vhdl -library work $origin_dir/../../sources/src/mgt_gbt_fpga/xlx_k7v7_mgt_ip_auto_phase_align.vhd
read_vhdl -library work $origin_dir/../../sources/src/mgt_gbt_fpga/xlx_k7v7_mgt_ip_cpll_railing.vhd
read_vhdl -library work $origin_dir/../../sources/src/mgt_gbt_fpga/xlx_k7v7_mgt_ip_gt.vhd
read_vhdl -library work $origin_dir/../../sources/src/mgt_gbt_fpga/xlx_k7v7_mgt_ip_init.vhd
read_vhdl -library work $origin_dir/../../sources/src/mgt_gbt_fpga/xlx_k7v7_mgt_ip_multi_gt.vhd
read_vhdl -library work $origin_dir/../../sources/src/mgt_gbt_fpga/xlx_k7v7_mgt_ip_rx_startup_fsm.vhd
read_vhdl -library work $origin_dir/../../sources/src/mgt_gbt_fpga/xlx_k7v7_mgt_ip_sync_block.vhd
read_vhdl -library work $origin_dir/../../sources/src/mgt_gbt_fpga/xlx_k7v7_mgt_ip_sync_pulse.vhd
read_vhdl -library work $origin_dir/../../sources/src/mgt_gbt_fpga/xlx_k7v7_mgt_ip_tx_manual_phase_align.vhd
read_vhdl -library work $origin_dir/../../sources/src/mgt_gbt_fpga/xlx_k7v7_mgt_ip_tx_startup_fsm.vhd
read_vhdl -library work $origin_dir/../../sources/src/mgt_gbt_fpga/xlx_k7v7_mgt_ip.vhd
read_vhdl -library work $origin_dir/../../sources/src/mgt_gbt_fpga/xlx_k7v7_mgt_latopt.vhd
read_vhdl -library work $origin_dir/../../sources/src/mgt_gbt_fpga/xlx_k7v7_mgt_std.vhd
read_vhdl -library work $origin_dir/../../sources/src/mgt_gbt_fpga/xlx_k7v7_reset.vhd


# mgt_main/packages
read_vhdl -library work $origin_dir/../../sources/src/mgt_gbt_fpga/packages/gbt_bank_package.vhd
read_vhdl -library work $origin_dir/../../sources/src/mgt_gbt_fpga/packages/xlx_k7v7_gbt_bank_package.vhd
read_vhdl -library work $origin_dir/../../sources/src/mgt_gbt_fpga/packages/xlx_k7v7_gbt_banks_user_setup.vhd

# IP cores
set obj [get_filesets sources_1]   
set files [list \
 "[file normalize "$origin_dir/../../sources/ip/xlx_k7v7_gbt_rx_frameclk_phalgnr_mmcm.xcix"]"\
 "[file normalize "$origin_dir/../../sources/ip/xlx_k7v7_tx_dpram.xcix"]"\
 "[file normalize "$origin_dir/../../sources/ip/xlx_k7v7_rx_dpram.xcix"]"\
 "[file normalize "$origin_dir/../../sources/ip/xlx_k7v7_tx_pll.xcix"]"\
 "[file normalize "$origin_dir/../../sources/ip/ila_gbt_fpga.xcix"]"\
 "[file normalize "$origin_dir/../../sources/ip/vio_gbt_fpga.xcix"]"\
]

add_files -norecurse -fileset $obj $files

# constraints
read_xdc -verbose $origin_dir/../../sources/xdc/gbt_fpga_vc709_pins.xdc
read_xdc -verbose $origin_dir/../../sources/xdc/gbt_fpga_vc709_timing.xdc

set_property target_language VHDL [current_project]

# Set 'sources_1' fileset properties
set obj [get_filesets sources_1]
set_property "top" "gbt_fpga_vc709_top" $obj

puts "############################################################################"
puts "Copyright Notice/Copying Permission:
    Copyright 2018 Christos Bakalis (christos.bakalis@cern.ch)\n

    This file is part of the GBT-FPGA_VC709 project.

    GBT-FPGA_VC709 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    GBT-FPGA_VC709 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with GBT-FPGA_VC709.  If not, see <http://www.gnu.org/licenses/>.\n"
puts "############################################################################"
puts "Special thanks to Dimitrios Matakias (dimitrios.matakias@cern.ch)
    and to Panagiotis Gkountoumis (panagiotis.gkountoumis@cern.ch) for their help.
    This design has been ported from the VC707 example design from CERN's GBT-FPGA
    group (https://gitlab.cern.ch/gbt-fpga).\n"
puts "############################################################################"

puts "###################################"
puts "INFO: Project created:$project_name"
puts "###################################"
puts "Build Succesful. Enjoy :)"
