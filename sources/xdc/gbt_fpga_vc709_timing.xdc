create_clock -period 6.400 -name USER_CLOCK [get_ports USER_CLOCK_P]
set_clock_groups -asynchronous -group USER_CLOCK

create_clock -period 8.333 -name MGT_REFCLK [get_ports SMA_MGT_REFCLK_N]
set_clock_groups -asynchronous -group MGT_REFCLK

## GBT TX:
##--------

## RX Phase Aligner
##-----------------
## Comment: The period of TX_FRAMECLK is 25ns but "TS_GBTTX_SCRAMBLER_TO_GEARBOX" is set to 16ns, providing 9ns for setup margin.
set_max_delay -datapath_only -from [get_pins -hier -filter {NAME =~ */*/*/*/scrambler/*/C}] -to [get_pins -hier -filter {NAME =~ */*/*/*/txGearbox/*/D}] 16.000

## GBT RX:
##--------

## Comment: The period of RX_FRAMECLK is 25ns but "TS_GBTRX_GEARBOX_TO_DESCRAMBLER" is set to 20ns, providing 5ns for setup margin.
set_max_delay -datapath_only -from [get_pins -hier -filter {NAME =~ */*/*/*/rxGearbox/*/C}] -to [get_pins -hier -filter {NAME =~ */*/*/*/descrambler/*/D}] 20.000
set_max_delay -datapath_only -from [get_pins -hier -filter {NAME =~ */*/*/*/rxGearbox/*/C}] -to [get_pins -hier -filter {NAME =~ */*/*/*/descrambler/*/D}] 20.000