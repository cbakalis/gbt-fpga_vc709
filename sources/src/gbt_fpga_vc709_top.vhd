----------------------------------------------------------------------------------
-- Company: NTU Athens - BNL
-- Engineer: Christos Bakalis (christos.bakalis@cern.ch) 
-- 
-- Copyright Notice/Copying Permission
--    Copyright 2018 Christos Bakalis (christos.bakalis@cern.ch)

--    This file is part of the GBT-FPGA_VC709 project.

--    GBT-FPGA_VC709 is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.

--    GBT-FPGA_VC709 is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.

--    You should have received a copy of the GNU General Public License
--    along with GBT-FPGA_VC709.  If not, see <http://www.gnu.org/licenses/>.
--    
--    Special thanks to Dimitrios Matakias (dimitrios.matakias@cern.ch)
--    and to Panagiotis Gkountoumis (panagiotis.gkountoumis@cern.ch) for their help.
--    This design has been ported from the VC707 example design from CERN's GBT-FPGA
--    group (https://gitlab.cern.ch/gbt-fpga).

--  
-- Create Date: 18.12.2018 17:54:56
-- Design Name: GBT-FPGA_VC709 Top Module
-- Module Name: gbt_fpga_vc709_top - RTL
-- Project Name: GBT-FPGA_VC709
-- Target Devices: Xilinx 7-Series FPGAs
-- Tool Versions: Vivado 2018.2
-- Description: GBT-FPGA implementation on a VC709
--
-- Dependencies:
-- 
-- Changelog:
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL; 
library UNISIM;
use UNISIM.VComponents.all;

-- Custom libraries and packages:
use work.gbt_bank_package.all;
use work.vendor_specific_gbt_bank_package.all;
use work.gbt_banks_user_setup.all;

entity gbt_fpga_vc709_top is
    port(
        ---------------------------------
        ------ General Interface --------
        GPIO_SW             : IN  STD_LOGIC_VECTOR(4 DOWNTO 0); -- VC709 switchbuttons
        GPIO_DIP            : IN  STD_LOGIC_VECTOR(7 DOWNTO 0); -- VC709 DIP switches
        CPU_RST             : IN  STD_LOGIC; -- CPU RST (assert if MGT clock is lost)
        USER_CLOCK_P        : IN  STD_LOGIC; -- raw 156.250 clock from VC709 for MGT's DRP (no buffer)
        USER_CLOCK_N        : IN  STD_LOGIC; -- raw 156.250 clock from VC709 for MGT's DRP (no buffer)
        SMA_MGT_REFCLK_P    : IN  STD_LOGIC; -- clean 120.237 clock from clock board
        SMA_MGT_REFCLK_N    : IN  STD_LOGIC; -- clean 120.237 clock from clock board
        GPIO_LED            : OUT STD_LOGIC_VECTOR(7 DOWNTO 0); -- VC709 status LEDs
        ---------------------------------
        --------- SFP Interface ---------
        SFP_RX_P            : IN  STD_LOGIC; -- SFP cage P4
        SFP_RX_N            : IN  STD_LOGIC; -- SFP cage P4
        SFP_TX_P            : OUT STD_LOGIC; -- SFP cage P4
        SFP_TX_N            : OUT STD_LOGIC; -- SFP cage P4
        SFP_TX_DISABLE      : OUT STD_LOGIC  -- SFP cage P4
    );
end gbt_fpga_vc709_top;

architecture RTL of gbt_fpga_vc709_top is

component gbt_fpga_wrapper
    port(
        -----------------------------------
        -------- General Interface --------
        cpu_reset           : in  std_logic; -- CPU_RST of VC709 (assert if clock loss)
        user_clock_p        : in  std_logic; -- raw 156.250 clock from VC709 for MGT's DRP (no buffer)
        user_clock_n        : in  std_logic; -- raw 156.250 clock from VC709 for MGT's DRP (no buffer)
        sma_mgt_refclk_p    : in  std_logic; -- clean 120.237 clock from clock board
        sma_mgt_refclk_n    : in  std_logic; -- clean 120.237 clock from clock board
        mgt_ready           : out std_logic; -- mgt_ready signal
        gbt_rx_ready        : out std_logic; -- RX-side ready
        pll_lock            : out std_logic; -- e-link PLL is locked
        -----------------------------------
        ---- GBT-FPGA Data Interface ------
        gbt_fpga_tx         : in  std_logic_vector(83 downto 0);
        gbt_fpga_rx         : out std_logic_vector(83 downto 0);
        -----------------------------------
        --- Clock-Forwarding Interface ----
        userclk             : out std_logic; -- raw 156.250 clock (after global buffer)
        clk_40              : out std_logic; -- e-link clock
        clk_80              : out std_logic; -- e-link clock
        clk_160             : out std_logic; -- e-link clock
        clk_320             : out std_logic; -- e-link clock
        -----------------------------------
        ---------- SFP Interface ----------
        sfp_rx_p            : in  std_logic;    
        sfp_rx_n            : in  std_logic;
        sfp_tx_p            : out std_logic;
        sfp_tx_n            : out std_logic
    );
end component;

component debouncer
  generic(
    is_testbench : std_logic := '1';
    nb_of_inputs : natural   := 8);
  port(
    ---------------------------------
    ------ General Interface --------
    clk         : in  std_logic;
    rstn        : in  std_logic;
    dbnc_in     : in  std_logic_vector(nb_of_inputs-1 downto 0);
    dbnc_out    : out std_logic_vector(nb_of_inputs-1 downto 0)
  );
end component;

component ila_gbt_fpga
    port(
        clk     : in std_logic;
        probe0  : in std_logic_vector(83 downto 0)
    );
end component;

component vio_gbt_fpga
    port(
        clk         : in std_logic;
        probe_in0   : in std_logic_vector(15 downto 0);
        probe_in1   : in std_logic_vector(15 downto 0);
        probe_in2   : in std_logic_vector(15 downto 0);
        probe_in3   : in std_logic_vector(15 downto 0);
        probe_in4   : in std_logic_vector(15 downto 0);
        probe_in5   : in std_logic_vector(15 downto 0);
        probe_in6   : in std_logic_vector(15 downto 0);
        probe_in7   : in std_logic_vector(15 downto 0)
    );
end component;

    signal userclk          : std_logic := '0';
    signal clk_40           : std_logic := '0';
    signal clk_80           : std_logic := '0';
    signal clk_160          : std_logic := '0';
    signal clk_320          : std_logic := '0';

    signal gpio_led_i       : std_logic_vector(7 downto 0) := (others => '0');
    signal gpio_dip_i       : std_logic_vector(7 downto 0) := (others => '0');
    signal gpio_sw_i        : std_logic_vector(4 downto 0) := (others => '0');
    signal gpio_sw_dbnc     : std_logic_vector(4 downto 0) := (others => '0');

    signal mgt_ready        : std_logic := '0';
    signal gbt_rx_ready     : std_logic := '0';
    signal elink_pll_lock   : std_logic := '0';
    signal elink_pll_lock_i : std_logic := '0';
    signal cpu_rst_i        : std_logic := '0';
    signal rst_stat         : std_logic := '0';

    signal gbt_fpga_tx      : std_logic_vector(83 downto 0) := (others => '0');
    signal gbt_fpga_rx      : std_logic_vector(83 downto 0) := (others => '0');
    signal ttc_in           : std_logic_vector(7 downto 0)  := (others => '0');

    type cnt_array is array (7 downto 0) of unsigned(15 downto 0);
    signal bit_cnt : cnt_array;


    attribute mark_debug                    : string;
    attribute mark_debug of gbt_fpga_tx     : signal is "TRUE";
    attribute mark_debug of gbt_fpga_rx     : signal is "TRUE";

    attribute async_reg                     : string;
    attribute async_reg of elink_pll_lock_i : signal is "TRUE";

begin

sync_proc: process(clk_40)
begin
  if(rising_edge(clk_40))then
    elink_pll_lock_i <= elink_pll_lock;
  end if;
end process;

cnt_gen: for I in 0 to 7 generate

stat_proc: process(clk_40)
begin
  if(rising_edge(clk_40))then
    if(rst_stat = '1')then
      bit_cnt(I) <= (others => '0');
    elsif(ttc_in(I) = '1')then
      bit_cnt(I) <= bit_cnt(I) + 1;
    else
      bit_cnt(I) <= bit_cnt(I);
    end if;
  end if;
end process;

end generate cnt_gen;

gbt_fpga_wrapper_inst: gbt_fpga_wrapper
    port map(
        -----------------------------------
        -------- General Interface --------
        cpu_reset           => cpu_rst_i,
        user_clock_p        => USER_CLOCK_P,
        user_clock_n        => USER_CLOCK_N,
        sma_mgt_refclk_p    => SMA_MGT_REFCLK_P,
        sma_mgt_refclk_n    => SMA_MGT_REFCLK_N,
        mgt_ready           => mgt_ready,
        gbt_rx_ready        => gbt_rx_ready,
        pll_lock            => elink_pll_lock,
        -----------------------------------
        ---- GBT-FPGA Data Interface ------
        gbt_fpga_tx         => gbt_fpga_tx,
        gbt_fpga_rx         => gbt_fpga_rx,
        -----------------------------------
        --- Clock-Forwarding Interface ----
        userclk             => userclk,
        clk_40              => clk_40,
        clk_80              => clk_80,
        clk_160             => clk_160,
        clk_320             => clk_320,
        -----------------------------------
        ---------- SFP Interface ----------
        sfp_rx_p            => SFP_RX_P,
        sfp_rx_n            => SFP_RX_N,
        sfp_tx_p            => SFP_TX_P,
        sfp_tx_n            => SFP_TX_N
    );

debouncer_inst: debouncer
  generic map(
    is_testbench => '0',
    nb_of_inputs => 5)
  port map(
    ---------------------------------
    ------ General Interface --------
    clk         => clk_40,
    rstn        => elink_pll_lock_i,
    dbnc_in     => gpio_sw_i,
    dbnc_out    => gpio_sw_dbnc
  );

ila_gbt_fpga_inst: ila_gbt_fpga
    port map(
        clk     => clk_40,
        probe0  => gbt_fpga_rx
    );

vio_gbt_fpga_inst: vio_gbt_fpga
    port map(
        clk         => clk_40,
        probe_in0   => std_logic_vector(bit_cnt(0)),
        probe_in1   => std_logic_vector(bit_cnt(1)),
        probe_in2   => std_logic_vector(bit_cnt(2)),
        probe_in3   => std_logic_vector(bit_cnt(3)),
        probe_in4   => std_logic_vector(bit_cnt(4)),
        probe_in5   => std_logic_vector(bit_cnt(5)),
        probe_in6   => std_logic_vector(bit_cnt(6)),
        probe_in7   => std_logic_vector(bit_cnt(7))
    );

-- i/o buffers
gen_buffers_led: for I in 0 to 7 generate
led_obuf:    OBUF  port map (O => GPIO_LED(I), I => gpio_led_i(I));
end generate gen_buffers_led;

gen_buffers_sw: for I in 0 to 4 generate
sw_buf:      IBUF  port map (O => gpio_sw_i(I), I => GPIO_SW(I));
end generate gen_buffers_sw;

gen_buffers_dip: for I in 0 to 7 generate
dip_buf:    IBUF  port map (O => gpio_dip_i(I), I => GPIO_DIP(I));
end generate gen_buffers_dip;

cpuRst_buf:     IBUF  port map (O => cpu_rst_i, I => CPU_RST);
txDisable_buf:  OBUF  port map (O => SFP_TX_DISABLE, I => '0');

    gpio_led_i(0)           <= elink_pll_lock;
    gpio_led_i(1)           <= mgt_ready;
    gpio_led_i(2)           <= gbt_rx_ready;

    gpio_led_i(7 downto 3)  <= (others => '0');

    rst_stat                <= gpio_sw_dbnc(3);
    ttc_in                  <= gbt_fpga_rx(11 downto 4); --Egroup4 EPATH5
        
end RTL;

