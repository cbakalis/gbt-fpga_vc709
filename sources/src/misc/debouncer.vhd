----------------------------------------------------------------------------------
-- Company: NTU Athens - BNL
-- Engineer: Christos Bakalis (christos.bakalis@cern.ch) 
-- 
-- Copyright Notice/Copying Permission:
--    Copyright 2019 Christos Bakalis
--
--    This file is part of the SCA_eXtension_firmware (SCAX).
--
--    SCAX is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    SCAX is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with SCAX.  If not, see <http://www.gnu.org/licenses/>.
--  
-- Create Date: 24.09.2018 13:45:47
-- Design Name: Debouncer
-- Module Name: debouncer - RTL
-- Project Name: SCAX
-- Target Devices: Xilinx 7-Series FPGAs
-- Tool Versions: Vivado 2018.2
-- Description: DIP switch debouncer
-- 
-- Dependencies:
-- 
-- Changelog:
--
----------------------------------------------------------------------------------
library UNISIM; 
library IEEE; 
use IEEE.NUMERIC_STD.ALL; 
use IEEE.STD_LOGIC_1164.ALL; 
use UNISIM.VComponents.all;

entity debouncer is
  generic(
    is_testbench : std_logic := '1';
    nb_of_inputs : natural   := 8);
  port(
    ---------------------------------
    ------ General Interface --------
    clk         : in  std_logic;
    rstn        : in  std_logic;
    dbnc_in     : in  std_logic_vector(nb_of_inputs-1 downto 0);
    dbnc_out    : out std_logic_vector(nb_of_inputs-1 downto 0)
  );
end debouncer;

architecture RTL of debouncer is

component CDCC
  generic(
    NUMBER_OF_BITS : integer := 8); -- number of signals to be synced
  port(
    clk_src    : in  std_logic;                                     -- input clk (source clock)
    clk_dst    : in  std_logic;                                     -- input clk (dest clock)
    data_in    : in  std_logic_vector(NUMBER_OF_BITS - 1 downto 0); -- data to be synced
    data_out_s : out std_logic_vector(NUMBER_OF_BITS - 1 downto 0)  -- synced data to clk_dst
  ); 
end component; 

  signal dbnc_in_i : std_logic_vector(nb_of_inputs-1 downto 0) := (others => '0');

  type stateType is (ST_LOW, ST_LOW_TO_HIGH, ST_HIGH_TO_LOW, ST_HIGH);
  type state_array is array (7 downto 0) of stateType; 
  type cnt_array is array (7 downto 0) of unsigned(15 downto 0);

  signal dbnc_cnt : cnt_array;

  signal state : state_array;

  signal dbnc_lim : std_logic_vector(15 downto 0) := x"FFFF";
   

begin

debounce_inst: for I in 0 to (nb_of_inputs-1) generate

-- debouncing process
dbnc_proc: process(clk)
begin
  if(rising_edge(clk))then
    if(rstn = '0')then
      dbnc_out(I) <= '0';
      dbnc_cnt(I) <= (others => '0');
      state(I)    <= ST_LOW;
    else
      case state(I) is

        -- now low
        when ST_LOW =>
          dbnc_out(I) <= '0';
          dbnc_cnt(I) <= (others => '0');

          if(dbnc_in_i(I) = '1')then
            state(I) <= ST_LOW_TO_HIGH;
          else
            state(I) <= ST_LOW;
          end if;

        -- transitioning to high
        when ST_LOW_TO_HIGH =>
          dbnc_out(I) <= '0';

          if(dbnc_in_i(I) = '1')then
            dbnc_cnt(I) <= dbnc_cnt(I) + 1;
          else
            dbnc_cnt(I) <= (others => '0');
          end if;

          if(dbnc_in_i(I) = '1' and dbnc_cnt(I) = unsigned(dbnc_lim))then
            state(I) <= ST_HIGH; -- its indeed high
          elsif(dbnc_in_i(I) = '1' and dbnc_cnt(I) < unsigned(dbnc_lim))then
            state(I) <= ST_LOW_TO_HIGH; -- keep counting
          else
            state(I) <= ST_LOW; -- oops
          end if; 


        -- now high
        when ST_HIGH =>
          dbnc_out(I) <= '1';
          dbnc_cnt(I) <= (others => '0');

          if(dbnc_in_i(I) = '0')then
            state(I) <= ST_HIGH_TO_LOW;
          else
            state(I) <= ST_HIGH;
          end if;

        -- transitioning to low
        when ST_HIGH_TO_LOW =>
          dbnc_out(I) <= '1';

          if(dbnc_in_i(I) = '0')then
            dbnc_cnt(I) <= dbnc_cnt(I) + 1;
          else
            dbnc_cnt(I) <= (others => '0');
          end if;

          if(dbnc_in_i(I) = '0' and dbnc_cnt(I) = unsigned(dbnc_lim))then
            state(I) <= ST_LOW; -- its indeed low
          elsif(dbnc_in_i(I) = '0' and dbnc_cnt(I) < unsigned(dbnc_lim))then
            state(I) <= ST_HIGH_TO_LOW; -- keep counting
          else
            state(I) <= ST_HIGH; -- oops
          end if;       

        when others =>
          dbnc_out(I) <= '0';
          dbnc_cnt(I) <= (others => '0');
          state(I)    <= ST_LOW;

      end case;
    end if;
  end if;
end process;

end generate debounce_inst;

CDCC_inst: CDCC
  generic map(
    NUMBER_OF_BITS => nb_of_inputs)
  port map(
    clk_src    => clk,
    clk_dst    => clk,
    data_in    => dbnc_in,
    data_out_s => dbnc_in_i
  ); 

  dbnc_lim <= x"1000" when is_testbench = '1' else x"FFFF";

end RTL;